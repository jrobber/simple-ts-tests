let path = require("path"),
  wallabyWebpack = require("wallaby-webpack");

module.exports = function(wallaby) {
  return {
    files: ["logic/**/*.ts"],

    tests: ["spec/**/*.spec.ts"],

    compilers: {
      '**/*.ts': wallaby.compilers.babel({
        presets: ['esnext']
      })
    },

    debug: true
  };
};